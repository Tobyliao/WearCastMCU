//SD Card
#include <LSD.h>

//BlueTooth Device
#include <LBT.h> 
#include <LBTServer.h>

//Temp and humi sensor
#include "LDHT.h"         // the library of the Temp/humi sensor
#define DHTPIN 7          // what pin we're connected to
#define DHTTYPE DHT22     // using DHT11 sensor
LDHT dht(DHTPIN,DHTTYPE);
float tempC=0.0,Humi=0.0;

//wifi
#include "LTask.h" 
#include "LWiFi.h" 
#include "LWiFiClient.h"

#define WIFI_AP "iPhone"
#define WIFI_PWD "donedone"
#define WIFI_AUTH LWIFI_WPA  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.
#define SITE_URL "tobyliao.com.tw"
LWiFiClient c; // client-end
 
//LCD
#include <LiquidCrystal.h>
LiquidCrystal lcd(9,6,5,4,3,2);      //Build up a LiquidCrystal class by using IO : 9,7,5,4,3,2

//Dust sensor
int DustPin = 8;
long OnTime = 2000;           // milliseconds of on-time
long OffTime = 3000;          // milliseconds of off-time
unsigned long previousMillis = 0;
unsigned long page = 1;
unsigned long duration;
unsigned long starttime;
unsigned long sampletime_ms = 3000;   //sampe 3s ;
unsigned long lowpulseoccupancy = 0;
float ratio = 0.0;
float concentration = 0.0;

//Light sensor
int lightPin=A0;
float lightvalue;

void setup()
{
  Serial.begin(9600);
  lcd.begin(16,2);             //Initialize LCD1602
  dht.begin();                 //start up the library dht
  LSD.begin();
  LWiFi.begin();
  LBTServer.begin((uint8_t*)"BT_Device");
  starttime = millis();        //get the current time;
  pinMode(DustPin,INPUT);      //dust sensor input pin
  pinMode(lightPin,INPUT);     //Sound sensor input pin
  lcd.print("Linkit ONE!");    //LCD show Linkit ONE!
  delay(1000);                 //delay 1000ms
  lcd.clear();                 //clear the monitor
   
  //SD Card
  Serial.print("Initializing SD card...");
  Serial.println("card initialized.");
  
  //Wifi
  Serial.println("Connecting to AP");
  while(LWiFi.connect(WIFI_AP,LWiFiLoginInfo(WIFI_AUTH,WIFI_PWD)) == 0){
    delay(1000);   //keep trying
    }
  
  Serial.println("Connecting to Website");
  while(c.connect(SITE_URL, 80) == 0){
    Serial.println("Reconnecting to website");
    delay(1000);
    }

  // send HTTP request, ends with 2 CR/LF
  Post_Json();
}

 
void loop()
{ 
  boolean disconnectMsg = false;
  if(dht.read()){
      sensor_setting();
      LCD_Monitor();
      Post_Json();
      String dataString = "";
      dataString = SD_data(dataString);
      //Serial.println(dataString);
      LFile dataFile = LSD.open("datalog.txt", FILE_WRITE); 
      
      if (dataFile) {
        dataFile.println(dataString);
        dataFile.close();
        Serial.println(dataString);
        }else{
          Serial.println("error opening datalog.txt");
          }
 
      if(LBTServer.connected()){
        if(dht.read()){
          LBTServer.print(dataString);
        }
      }
      
    else{
      Serial.println("Retrying");   //Else retry
      LBTServer.accept(5);
    }
  }

  Post_Json();
  // httpStatusCheck(disconnectMsg);



  
}



void Post_Json(){
  String PostData = Json_build();
  Serial.println("Connected. Sending HTTP POST Request ...."); 
  c.println("POST /project_website/sqlInsert.php HTTP/1.1");           //define POST path
  c.println("Host: " SITE_URL);
  c.println("Connection: close");
  c.println("Content-Type: application/json");     //define Content-Type
  c.print("Content-Length: ");
  c.println(PostData.length());
  c.println();
  
  c.println(PostData);  //send the HTTP POST body
  Serial.println(PostData); 

  // waiting for server response
  while (!c.available())
  {
    delay(100);
  }
}


  
//sensor_setting
void sensor_setting(){
  unsigned long currentMillis = millis();
  lightvalue = analogRead(lightPin);
  tempC = dht.readTemperature();
  Humi = dht.readHumidity();
  duration = pulseIn(DustPin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;
  
  if ((millis()-starttime) > sampletime_ms){                            //if the sampel time == 30s
      ratio = lowpulseoccupancy/(sampletime_ms*10.0);                   // Integer percentage 0=>100
      concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
      lowpulseoccupancy = 0;
      starttime = millis();
  }
  lightvalue = analogRead(lightPin);
  tempC = dht.readTemperature();
  Humi = dht.readHumidity();
  duration = pulseIn(DustPin, LOW);
  lowpulseoccupancy = lowpulseoccupancy+duration;
  
  if ((millis()-starttime) > sampletime_ms){                            //if the sampel time == 30s
      ratio = lowpulseoccupancy/(sampletime_ms*10.0);                   // Integer percentage 0=>100
      concentration = 1.1*pow(ratio,3)-3.8*pow(ratio,2)+520*ratio+0.62; // using spec sheet curve
      lowpulseoccupancy = 0;
      starttime = millis();
  }
}

//LCD_Monitor
void LCD_Monitor(){
  unsigned long currentMillis = millis();
  if((page == 1) && (currentMillis - previousMillis >= OnTime))
        {
          page = 2;  // Change page 2
          previousMillis = currentMillis;  // Remember the time 
          lcd.clear();              //clear the monitor
          lcd.setCursor(0,0);       //set the pointer to the certain place
          lcd.print("con=");      //Monitor show“Humi =”
          lcd.print(concentration);     //Monitor show Humidity's value
          lcd.print("pcs/.01cf");         //Monitor show“%RH”
          lcd.setCursor(0,1);       //set the pointer to the certain place 
          lcd.print("Light =");      //Monitor show“Sound =”
          lcd.print(lightvalue);    //Monitor show Sound value
        }
      else if ((page == 2) && (currentMillis - previousMillis >= OffTime))
        {
          page = 1;  // Change page 1
          previousMillis = currentMillis;   // Remember the time
          lcd.clear();              //clear the monitor
          lcd.setCursor(0,0);       //set the pointer to the certain place
          lcd.print("Temp =");      //Monitor show“Temp =”
          lcd.print((int)tempC);    //Monitor show Temperature value
          lcd.print(".");           //Monitor show point
          lcd.print((int)tempC%10); //Monitor show value behind point      
          lcd.print((char)223);     //Monitor show“°”
          lcd.print("C");           //Monitor show“C”
          lcd.setCursor(0,1);       //set the pointer to the certain place
          lcd.print("Humi =");      //Monitor show“Humi =”
          lcd.print((int)Humi);     //Monitor show Humidity's value
          lcd.print("%RH");         //Monitor show“%RH”
       }
  }




//SD_Storage
String SD_data(String dataString){

  float sensor =  tempC ;
  dataString += String(sensor);
  dataString += ","; 
  sensor =  Humi ;
  dataString += String(sensor);
  dataString += ",";
  sensor =  concentration ;
  dataString += String(sensor);
  dataString += ",";
  sensor =  lightvalue;
  dataString += String(sensor);
  dataString += "/n";
  return dataString;
}

//create json doc
String Json_build(){
    String data = "{";
    data+="\n";
    data+= "\"d\": {";
    data+="\n";
    data+="\"temperature\": ";
    data+=tempC;
    data+= ",";
    data+="\n";
    data+="\"humidity\": ";
    data+=Humi;
    data+= ",";
    data+="\n";
    data+="\"pm2.5\": ";
    data+=concentration;
    data+= ",";
    data+="\n";
    data+="\"lightvalue\": ";
    data+=lightvalue;
    data+="\n";
    data+="}";
    data+="\n";
    data+="}";
    return data;
  }

void httpStatusCheck(boolean disconnectedMsg){
    // Make sure we are connected, and dump the response content to Serial
    while (c)
    {
      int v = c.read();
      if (v != -1)
      {
        Serial.print((char)v);
      }// Make sure we are connected, and dump the response content to Serial
    
    else
    {
      Serial.println("no more content, disconnect");
      c.stop();
      while (1)
      {
        delay(1);
      }
    }
    if (!disconnectedMsg)
    {
      Serial.println("disconnected by server");
      disconnectedMsg = true;
    }
    delay(500);
    }
  }



